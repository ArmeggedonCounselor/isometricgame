﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : Pickup 
{
    public GameObject weaponPrefab;

    public override void OnPickup(GameObject target) {
        GameObject weapon = Instantiate(weaponPrefab);
        Weapon temp = weapon.GetComponent<Weapon>();
        Pawn pawn = target.GetComponent<Pawn>();
        if(pawn != null) {
            temp.OnPickup(pawn);
        }
        Destroy(this.gameObject, 0.001f);
    }
}
