﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandminePickup : Pickup {
    public float amount;

    public override void OnPickup(GameObject target)
    {
        Health targetHealth = target.GetComponent<Health>();
        if (targetHealth == null) return;
        targetHealth.TakeDamage(amount);
        base.OnPickup(target);
    }

}