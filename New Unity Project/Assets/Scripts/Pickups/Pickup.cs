﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour {

    private Transform tf;
    public float rotateSpeed;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Spin();
	}

    void Spin()
    {
        tf.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
    }

    public virtual void OnPickup(GameObject target)
    {
      
        // Whatever we want every pickup to do goes in here.

        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        OnPickup(other.gameObject);
    }
}
