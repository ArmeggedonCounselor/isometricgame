﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : Pickup {

    public float amount;

    public override void OnPickup(GameObject target)
    {
        Health targetHealth = target.GetComponent<Health>();
        if(targetHealth == null)
        {
            return;
        }
        targetHealth.Heal(amount);
        base.OnPickup(target);
    }
}
