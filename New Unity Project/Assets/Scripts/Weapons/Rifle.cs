﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Weapon {
    public override void OnPickup(Pawn target) {
        if(target.rifle == null) {
            target.rifle = this;
            this.gameObject.layer = target.gameObject.layer;
            if(target.wielded == null) {
                target.EquipWeapon(this);
            } else {
                OnSwitch(target);
            }
        } else {
            // In the future, we'll put code for holding a button to switch the held weapon.
            // For now, nothing happens.
        }
    }

    public override void OnEquip(Pawn target) {
        base.OnEquip(target);
        tf.SetPositionAndRotation(target.rifleWeaponSocket.position, target.rifleWeaponSocket.rotation);
        tf.SetParent(target.rifleWeaponSocket);
    }

    public override bool OnSwitch(Pawn target) {
        if (target.wielded == this) {
            target.wielded = null;
        }
        tf.SetPositionAndRotation(target.shoulderHolster.position, target.shoulderHolster.rotation);
        tf.SetParent(target.shoulderHolster);
        return true;
    }
}
