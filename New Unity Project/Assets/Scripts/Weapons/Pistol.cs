﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon {
    public override void OnPickup(Pawn target) {
        if (target.sidearm == null) {
            target.sidearm = this;
            this.gameObject.layer = target.gameObject.layer;
            if (target.wielded == null) {
                target.EquipWeapon(this);
            } else {
                this.OnSwitch(target);
            }
        } else {
            // In the future, we'll put code for holding a button to switch the held weapon.
            // For now, nothing happens.
        }
    }

    public override void OnEquip(Pawn target) {
        base.OnEquip(target);
        tf.SetPositionAndRotation(target.pistolWeaponSocket.position, target.pistolWeaponSocket.rotation);
        tf.SetParent(target.pistolWeaponSocket);
    }

    public override bool OnSwitch(Pawn target) {
        if (target.wielded == this) {
            target.wielded = null;
        }
        tf.SetPositionAndRotation(target.pistolHolster.position, target.pistolHolster.rotation);
        tf.SetParent(target.pistolHolster);
        return true;
    }
}
