﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Weapon {
    [HideInInspector]public bool swordSheathed;
    [HideInInspector]public bool repeatSwing;

    public Collider swordHitBox;

    public override void OnPickup(Pawn target) {
        if (target.sword == null) {
            target.sword = this;
            if (target.wielded == null) {
                target.EquipWeapon(this);
            } else {
                swordSheathed = true;
                this.OnSwitch(target);
            }
        } else {
            // In the future, we'll put code for holding a button to switch the held weapon.
            // For now, nothing happens.
        }
    }

    public override bool OnSwitch(Pawn target) {
        if (!swordSheathed) {
            DoneSwitching = false;
            StartCoroutine(WaitingRoutine(target));
            return DoneSwitching;
        }
        tf.SetPositionAndRotation(target.swordSheath.position, target.swordSheath.rotation);
        tf.SetParent(target.swordSheath);
        swordSheathed = true;
        DoneSwitching = true;
        return DoneSwitching;
    }

    public override void Fire(Pawn owner) {       
            owner.anim.SetTrigger("Slash");
        
    }

    public override void OnEquip(Pawn target) {
        target.LeftHand = null;
        target.RightHand = null;
        if (swordSheathed) {
            StartCoroutine(WaitingRoutine(target));
            return;
        }
        target.wielded = this;
        tf.SetParent(target.swordHand);
        tf.SetPositionAndRotation(target.swordHand.position, target.swordHand.rotation);
    }

    public void HitBox(string state) {
        switch (state) {
            case "On": {
                    swordHitBox.enabled = true;
                    break;
                }
            case "Off": {
                    swordHitBox.enabled = false;
                    break;
                }
        }
    }

    public void OnTriggerEnter(Collider other) {
        Pawn target = other.gameObject.GetComponent<Pawn>();
        if(target != null) {
            target.health.TakeDamage(damage);
        }
    }

    IEnumerator WaitingRoutine(Pawn target) {
        if (swordSheathed) {
            while (swordSheathed) {
                yield return null;
            }
            OnEquip(target);            
        } else {
            while (!swordSheathed) {
                yield return null;
            }
            OnSwitch(target);
        }
    }
}
