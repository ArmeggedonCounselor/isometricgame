﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {
    [Header("Game Objects")]
    public GameObject bulletPrefab;

    [Header("Transforms")]
    public Transform firePoint;
    public Transform LHPoint;
    public Transform RHPoint;

    [HideInInspector]
    public Transform tf;

    [Header("Weapon Properties")]
    public float damage;
    public float shotForce;
    public float spread;
    public float shotsPerSecond;
    public WeaponType type;

    private float shotTimer;
    [Tooltip("This is how fast the shotTimer returns to 0 when the gun stops firing.")]
    public float resetTimer = 0.2f;
    public float resetTime;
    private bool reset;   

    protected bool DoneSwitching = true;
    public bool Switched {
        get {
            return DoneSwitching;
        }
    }

    // UI stuff would go under here if I had anything for it.

    private void Awake() {

        tf = GetComponent<Transform>();
    }

    void Start() {
        shotTimer = 0f;
        resetTime = resetTimer;
    }

    void Update() {
        if (reset) {
            resetTime -= Time.deltaTime;
            if(resetTime <= 0f) {
                shotTimer = 0f;
                resetTime = resetTimer;
                reset = false;
            }
        }
    }

    public virtual void OnEquip(Pawn target) {
        target.wielded = this;
        if (RHPoint != null) {
            target.RightHand = RHPoint;
        }
        if (LHPoint != null) {
            target.LeftHand = LHPoint;
        }        
    }

    public abstract bool OnSwitch(Pawn target);

    public abstract void OnPickup(Pawn target);

    public virtual void Fire(Pawn owner) {
        if(bulletPrefab != null) {
            reset = false;
            resetTime = resetTimer;
            if (shotTimer <= 0f) {                
                GameObject newBullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation * Quaternion.Euler(Random.onUnitSphere * spread));
                newBullet.layer = this.gameObject.layer;
                Bullet bullet = newBullet.GetComponent<Bullet>();
                bullet.damage = damage;
                bullet.rb.AddForce(firePoint.forward * shotForce);
                shotTimer = 1 / shotsPerSecond;
            } else {
                shotTimer -= Time.deltaTime;
            }
        }
    }

    public virtual void CeaseFire() {
        reset = true;
    }
}

public enum WeaponType { Rifle, Pistol, Sword };
