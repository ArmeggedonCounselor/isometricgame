﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{

    [HideInInspector] public Transform tf;
    public Rigidbody rb;
    public float damage;
    public float lifespan;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, lifespan);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision) {
        if (collision.collider.gameObject.layer != this.gameObject.layer) {
            Pawn target = collision.collider.gameObject.GetComponent<Pawn>();
            if (target != null) {
                target.health.TakeDamage(damage);
            }
            Destroy(gameObject);
        }
    }
}
