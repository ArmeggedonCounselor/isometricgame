﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawners : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public List<GameObject> spawnedObjects;

    public int maxObjectsSpawned;
    public float spawnTime;
    public bool spawnOnStart;
    public bool NoRespawn;

    private float spawnTimer;
    private Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        spawnedObjects = new List<GameObject>();
        tf = GetComponent<Transform>();
        spawnTimer = spawnTime;
        if (spawnOnStart) {
            SpawnObject();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!NoRespawn) {
            if (spawnedObjects.Count < maxObjectsSpawned) {
                spawnTimer -= Time.deltaTime;
                if (spawnTimer <= 0f) {
                    SpawnObject();
                    spawnTimer = spawnTime;
                }
            }
            CheckToRemove();
        }
    }

    void SpawnObject() {
        GameObject newObject = Instantiate(prefabToSpawn, tf) as GameObject;
        spawnedObjects.Add(newObject);
    }

    void CheckToRemove() {
        List<int> indices = new List<int>();
        for(int i = 0; i < spawnedObjects.Count; i++) {
            if(spawnedObjects[i] == null) {
                indices.Add(i);
            }
        }
        for(int i = 0; i < indices.Count; i++) {
            spawnedObjects.RemoveAt(indices[i]);
        }
    }
}
