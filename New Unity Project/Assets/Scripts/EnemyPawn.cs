﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPawn : Pawn
{
    public override void Die() {
        Destroy(this.gameObject);
    }

    public override void EquipWeapon(Weapon weapon) {
        throw new System.NotImplementedException();
    }

    public override void FireWeapon() {
        throw new System.NotImplementedException();
    }

    public override void Move(Vector3 movement) {
        throw new System.NotImplementedException();
    }

    public override void Rotate(Vector3 lookPoint) {
        throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
