﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {
    public Text healthText;
    public Image healthBar;
    private Pawn target;
    [SerializeField]
    private float _health;
    public float health
    {
        get { return _health; }
        private set { _health += value; DisplayHealth(); }
    }
    [SerializeField]
    private float _maxHealth;
    public float MaxHealth
    {
        get { return _maxHealth; }
    }

	// Use this for initialization
	void Start () {
        _health = _maxHealth;
        target = GetComponent<Pawn>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void DisplayHealth()
    {
        if (healthText != null) {
            healthText.text = Mathf.RoundToInt(_health / _maxHealth * 100) + "%";
        }
        if (healthBar != null) {
            healthBar.fillAmount = _health / _maxHealth;
        }
    }

    public void TakeDamage(float amount)
    {
        _health -= amount;
        DisplayHealth();
        if(_health <= 0)
        {
            Die();
        }
    }

    public void Heal(float amount, bool canGoOverMax = false) {
        if ((!canGoOverMax && _health < _maxHealth) || canGoOverMax)
        {
            _health += amount;
            if (!canGoOverMax && _health > _maxHealth)
            {
                _health = _maxHealth;
            }
            DisplayHealth();
        }
    }

    public void Die()
    {
        target.Die();
    }
}
