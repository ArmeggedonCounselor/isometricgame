﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PlayerPawn pawn;
    public float walkMultiplier;
    public float sprintMultiplier;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Handle specific buttons.
        if (Input.GetButtonDown("Weapon Select Pistol")) {
            if(pawn.sidearm == null) {
                Debug.Log("Pistol!");
                return;
            }
            pawn.EquipWeapon(pawn.sidearm);
        }
        if(Input.GetButtonDown("Weapon Select Rifle")) {
            if(pawn.rifle == null) {
                Debug.Log("Rifle!");
                return;
            }
            pawn.EquipWeapon(pawn.rifle);
        }
        if(Input.GetButtonDown("Weapon Select Sword")) {
            if(pawn.sword == null) {
                Debug.Log("Sword!");
                return;
            }
            pawn.EquipWeapon(pawn.sword);
        }
        if (pawn.wielded != null) {
            if (pawn.wielded.GetType() != typeof(Sword) && Input.GetButton("Fire Weapon")) {
                pawn.FireWeapon();
            }
            if (pawn.wielded.GetType() == typeof(Sword) && Input.GetButtonDown("Fire Weapon")) {
                pawn.FireWeapon();
            }
            if (Input.GetButtonUp("Fire Weapon")) {
                pawn.wielded.CeaseFire();
            }
        }
        HandleRotation();
        HandleMovement();
    }

    public void HandleMovement() {
        Vector3 movementVector = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        movementVector = Vector3.ClampMagnitude(movementVector, 1f);
        movementVector = pawn.tf.InverseTransformDirection(movementVector);

        if (Input.GetButton("Sprint")) {
            movementVector *= sprintMultiplier;
        }
        if (Input.GetButton("Walk")) {
            movementVector *= walkMultiplier;
        }        

        pawn.Move(movementVector);
    }

    public void HandleRotation() {
        Plane groundPlane = new Plane(Vector3.up, pawn.tf.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        Vector3 lookPoint = Vector3.zero;
        if(groundPlane.Raycast(ray, out distance)) {
            lookPoint = ray.GetPoint(distance);
        }
        pawn.Rotate(lookPoint);
    }
}
