﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn {
    // Start is called before the first frame update
    void Start() {
        tf = GetComponent<Transform>();
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update() {
        
    }

    public override void Move(Vector3 movement) {
        anim.SetFloat("Horizontal", movement.x * runSpeed);
        anim.SetFloat("Vertical", movement.z * runSpeed);
    }

    public override void Rotate(Vector3 lookPoint) {
        //tf.LookAt(lookPoint);
        Quaternion lookRotate = Quaternion.LookRotation(lookPoint - tf.position, tf.up);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, lookRotate, turnSpeed * Time.deltaTime);
    }

    public override void EquipWeapon(Weapon weapon) {
        if (weapon != null) {
            anim.SetBool("PistolEquipped", false);
            anim.SetBool("RifleEquipped", false);
            anim.SetBool("SwordEquipped", false);
            Sword sword = wielded as Sword;
            Pistol pistol = wielded as Pistol;
            Rifle rifle = wielded as Rifle;            
            if (wielded != null) {
                if (!wielded.OnSwitch(this)) {
                    StartCoroutine(WaitRoutine(weapon));
                    return;
                }
            }
            switch (weapon.type) {
                case WeaponType.Pistol: {
                        anim.SetBool("PistolEquipped", true);
                        // Code to update UI.
                        break;
                    }
                case WeaponType.Rifle: {
                        anim.SetBool("RifleEquipped", true);
                        break;
                    }
                case WeaponType.Sword: {
                        anim.SetBool("SwordEquipped", true);
                        break;
                    }
            }
            weapon.OnEquip(this);
        }
    }

    IEnumerator WaitRoutine(Weapon weapon) {
        while (!wielded.Switched) {
            yield return null;
        }
        wielded = null;
        EquipWeapon(weapon);
    }

    public void HitBox(string state) {
        Sword temp = wielded as Sword;
        if (temp != null) {
            temp.HitBox(state);
        }
    }

    public void Slash(int state) {
        Sword temp = wielded as Sword;
        if(state == 0) {
            temp.repeatSwing = false;
        }
        if(state == 1) {
            temp.repeatSwing = true;
        }
    }

    public void DrawWeapon(string drawOrSheath) {
        Sword temp = sword as Sword;
        if(temp != null) {
            switch (drawOrSheath) {
                case "Draw": {
                        temp.swordSheathed = false;
                        break;
                    }
                case "Sheath": {
                        temp.swordSheathed = true;
                        break;
                    }
            }
        }
    }

    public override void FireWeapon() {
        if(wielded == null) {
            Debug.Log("Bang!");
            return;
        }
        wielded.Fire(this);
    }

    public override void Die() {
        // Something with ragdolls.
    }
}
