﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {
    public float runSpeed;
    public float turnSpeed;

    public Animator anim;
    public Health health;

    public Transform pistolHolster;
    public Transform shoulderHolster;
    public Transform swordSheath;
    public Transform swordHand;
    public Transform rifleWeaponSocket;
    public Transform pistolWeaponSocket;

    public Weapon sidearm;
    public Weapon rifle;
    public Weapon sword;

    public Weapon wielded;

    [HideInInspector] public Transform tf;
    [HideInInspector] public Transform RightHand;
    [HideInInspector] public Transform LeftHand;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract void Move(Vector3 movement);

    public abstract void Rotate(Vector3 lookPoint);

    public abstract void EquipWeapon(Weapon weapon);

    public abstract void FireWeapon();

    public abstract void Die();

    public void OnAnimatorIK(int layerIndex) {
        if(RightHand == null) {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0.0f);
        } else {
            anim.SetIKPosition(AvatarIKGoal.RightHand, RightHand.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, RightHand.rotation);
            
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
        }
        if(LeftHand == null) {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0.0f);
        } else {
            anim.SetIKPosition(AvatarIKGoal.LeftHand, LeftHand.position);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, LeftHand.rotation);
          
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);
        }
    }
}
